# Getting Started with Developer Test App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install`

Installs all dependencies.

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run test`

Launches the test runner in the interactive watch mode.
 
### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

### `serve -s build`

Serves the app in production mode from the `build` folder.
If you do not have serve installed please execute the script below to install it.
`npm install serve`

## Production Version
The Finished and deployed production version is available at
`https://freshcells.oakshieldtech.com/`