import React from 'react';
import {
  Container,
  Divider,
  Grid,
  Hidden,
  useMediaQuery,
  useTheme
} from '@material-ui/core';
import illustration1 from '../../assets/images/illustrations/pack1/authentication.svg';
import logo from '../../assets/images/freshcells_logo.svg';
import clsx from 'clsx';
import LoginForm from './forms/LoginForm';
import FreshCellsTitle from '../components/views/FreshCellsTitle';

const MyComponent = () => {
  const theme = useTheme();
  const isSM = useMediaQuery(theme.breakpoints.down('md'));
  return (
    <>
      <FreshCellsTitle title="Login" />

      <Grid className="app-wrapper bg-white min-vh-100">
        <Grid className="app-main min-vh-100">
          <Grid className="app-content pn">
            <Grid className="app-content--inner d-flex align-items-center">
              <Grid className="flex-grow-1 w-100 d-flex align-items-center">
                <Grid className="bg-composed-wrapper--content py-5">
                  <Container>
                    <Grid container spacing={6}>
                      <Grid
                        item
                        lg={6}
                        sm={12}
                        xs={12}
                        className="d-flex align-items-center">
                        <div className={clsx('w-100', { 'pr-lg-5': !isSM })}>
                          <div className="text-black mt-3">
                            <img
                              alt="..."
                              className="mx-auto d-block img-fluid"
                              src={logo}
                            />
                            <span className="text-center mt30">
                              <h1 className="display-4 mb-1 font-weight-bold">
                                Login to your account
                              </h1>
                            </span>

                            <LoginForm />
                          </div>
                        </div>
                      </Grid>

                      <Hidden mdDown={true}>
                        <Divider orientation="vertical" flexItem />
                        <Grid
                          item
                          lg={5}
                          className="d-none d-lg-flex align-items-center">
                          <img
                            alt="..."
                            className="w-100 mx-auto d-block img-fluid"
                            src={illustration1}
                          />
                        </Grid>
                      </Hidden>
                    </Grid>
                  </Container>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default MyComponent;
