import { FormikValues } from 'formik';

export interface ILoginFormFormikValues extends FormikValues {
  identifier: string;
  password: string;
}
