import React from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import FreshCellsTextField from '../../components/forms/FreshCellsTextField';
import { EmailRounded, LockRounded } from '@material-ui/icons';
import { Button, Grid } from '@material-ui/core';
import * as Yup from 'yup';
import * as OakValidator from '../../../utils/Validators';
import { useMutation } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import { LOGIN_ENDPOINT } from '../../../api/auth/mutations';
import {
  processResponseError,
  processServerError
} from '../../errors/ProcessServerError';
import tokenSlice from '../../../store/slices/auth/tokenSlice';
import profileSlice from '../../../store/slices/auth/profileSlice';
import { Redirect, useHistory } from 'react-router-dom';
import FreshCellsPasswordField from '../../components/forms/FreshCellsPasswordField';
import { ILoginFormFormikValues } from '../types/ILoginFormProps';
import { BeatLoader } from 'react-spinners';
import { IStoreStateProps } from '../../../store/types/IStoreStateProps';

const LoginForm = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const token = useSelector((state: IStoreStateProps) => state.token);
  const profile = useSelector((state: IStoreStateProps) => state.profile);

  const [doLogin] = useMutation(LOGIN_ENDPOINT, {
    errorPolicy: 'all',
    fetchPolicy: 'no-cache'
  });

  //SOME INITIAL VALUES FOR THE FORMIK FORM HOOK
  const initialValues = {
    identifier: '',
    password: ''
  };

  //THE YUP VALIDATION SCHEMA TO BE USED FOR VALIDATION
  const validationSchema = Yup.object().shape({
    identifier: OakValidator.validateEmail(true, 10, 200),
    password: OakValidator.validateString(true, 2, 50)
  });

  const submitForm = async (
    values: ILoginFormFormikValues,
    { setSubmitting, resetForm }: FormikHelpers<ILoginFormFormikValues>
  ) => {
    const variables = {
      identifier: values.identifier,
      password: values.password
    };

    const fetched = await doLogin({
      variables: variables
    }).catch((reason) => {
      processServerError(reason, dispatch);
    });

    if (fetched?.data) {
      const jwt = fetched?.data?.login?.jwt || '';
      if (jwt) {
        /*
          Dispatch The setToken action
          Dispatch The setLoggedIn action
        */
        dispatch(tokenSlice.actions.setToken(jwt));
        dispatch(profileSlice.actions.setLoggedIn(true));

        /*
          This marks the last time this person checked or was active
          This app automatically logs a user out after 2hrs of inactivity
        */
        localStorage['user-last-seen'] = Date.now();

        resetForm();
        setSubmitting(false);

        //Take the user to the profile page on successful login
        history.push('/account');
      }
    } else {
      processResponseError(fetched?.errors, dispatch);
    }
  };

  //if the user is already authenticated, then redirect the user to the dashboard.
  let isAuthenticated: boolean | undefined =
    token !== '' && token.length > 50 && profile.logged_in;

  if (isAuthenticated) {
    return <Redirect to="/account" />;
  }

  return (
    <React.Fragment>
      <Formik
        initialValues={initialValues}
        onSubmit={submitForm}
        validateOnChange={true}
        validateOnBlur={true}
        validationSchema={validationSchema}>
        {({ isSubmitting }) => (
          <Form className="" autoComplete="false">
            <div>
              <Grid container spacing={3} className="prn pln mt20 mb15">
                <Grid item md={12} sm={12} xs={12} lg={12}>
                  <FreshCellsTextField
                    name="identifier"
                    label="Email address"
                    readOnly={false}
                    placeholder="johndoe@freshcells.de"
                    icon={<EmailRounded />}
                  />
                </Grid>

                <Grid item md={12} sm={12} xs={12} lg={12}>
                  <FreshCellsPasswordField
                    name="password"
                    label="Password"
                    placeholder="Password"
                    readOnly={false}
                    icon={<LockRounded />}
                  />
                </Grid>
              </Grid>

              <Button
                size="large"
                fullWidth
                type="submit"
                disabled={isSubmitting}
                color="primary"
                variant="contained"
                className="text-uppercase font-weight-bold font-size-sm btn-primary">
                {isSubmitting ? (
                  <div className="pt5">
                    <BeatLoader color={'#070919'} loading={true} />
                  </div>
                ) : (
                  <>Sign in</>
                )}
              </Button>
            </div>

            <div className="mb30" />
          </Form>
        )}
      </Formik>
    </React.Fragment>
  );
};

export default LoginForm;
