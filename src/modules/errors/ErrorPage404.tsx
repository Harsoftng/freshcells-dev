import React from 'react';
import illustration2 from '../../assets/images/illustrations/pack4/404.svg';
import { Button, Grid } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

export default function ErrorPage404() {
  return (
    <React.Fragment>
      <Grid className="app-wrapper min-vh-100 bg-white">
        <Grid className="app-main min-vh-100">
          <Grid className="app-content p-0">
            <Grid className="app-inner-content-layout--main">
              <Grid className="flex-grow-1 w-100 d-flex align-items-center">
                <Grid className="bg-composed-wrapper--content">
                  <Grid container spacing={0} className="min-vh-100">
                    <Grid
                      item
                      md={12}
                      className="d-flex align-items-center justify-content-center">
                      <Grid
                        item
                        md={6}
                        xl={5}
                        sm={10}
                        xs={10}
                        className="mx-auto text-center">
                        <Grid className="py-4">
                          <img
                            src={illustration2}
                            className="w-50 mx-auto d-block mb-5 img-fluid"
                            alt="..."
                          />

                          <h1 className="display-1 mb-3 px-4 font-weight-bold">
                            404 Page Not Found
                          </h1>
                          <h3 className="font-size-xxl line-height-sm font-weight-light d-block px-3 mb-3 text-black-50">
                            The page you were looking for doesn't exist.
                          </h3>
                          <p>
                            It's on us, we may have probably moved the content
                            to a different page. Please click on the button
                            below to go back to your profile.
                          </p>
                          <p>
                            <Link to={'/account'}>
                              <Button
                                color="primary"
                                variant="contained"
                                className="btn-primary px-5 font-size-sm font-weight-bold btn-animated-icon text-uppercase rounded shadow-none py-3 hover-scale-sm hover-scale-lg">
                                <span className="btn-wrapper--label">
                                  Go to Profile
                                </span>
                                <span className="btn-wrapper--icon">
                                  <FontAwesomeIcon icon={['fas', 'home']} />
                                </span>
                              </Button>
                            </Link>
                          </p>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
