import React from 'react';
import { Button, Grid } from '@material-ui/core';
import illustration2 from '../../assets/images/illustrations/pack4/something_went_wrong.png';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: null, errorInfo: null };
    this.history = props.history;
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, errorInfo);
    this.setState({
      error: error,
      errorInfo: errorInfo
    });
  }

  render() {
    if (this.state.errorInfo) {
      // You can render any custom fallback UI
      return (
        <React.Fragment>
          <Grid className="app-wrapper min-vh-100 bg-white">
            <Grid className="app-main min-vh-100">
              <Grid className="app-content p-0">
                <Grid className="app-inner-content-layout--main">
                  <Grid className="flex-grow-1 w-100 d-flex align-items-center justify-content-center">
                    <Grid className="bg-composed-wrapper--content">
                      <Grid container spacing={0} className="min-vh-100">
                        <Grid
                          item
                          lg={12}
                          xl={12}
                          sm={12}
                          xs={12}
                          md={12}
                          className="d-flex align-items-center justify-content-center">
                          <Grid
                            item
                            md={8}
                            xl={6}
                            xs={10}
                            sm={10}
                            className=" text-center">
                            <div className="py-4">
                              <img
                                src={illustration2}
                                className="mx-auto d-block mb-5 img-fluid"
                                width={200}
                                alt="..."
                              />

                              <h1 className="display-1 mb-3 px-4 font-weight-bold">
                                Something went wrong
                              </h1>
                              <h3 className="font-size-xxl line-height-sm font-weight-light d-block px-3 mb-3 text-black-50">
                                There was an error, please try again later.
                              </h3>
                              <p>
                                The app encountered an internal error and was
                                unable to complete your request.
                              </p>
                              <p>
                                <Button
                                  onClick={() => {
                                    this.history.push('/account');
                                  }}
                                  color="primary"
                                  variant="contained"
                                  className="btn-warning mt20 px-5 font-size-sm font-weight-bold btn-animated-icon text-uppercase rounded shadow-none py-3 hover-scale-sm hover-scale-lg">
                                  <span className="btn-wrapper--label">
                                    Go to Profile
                                  </span>
                                  <span className="btn-wrapper--icon">
                                    <FontAwesomeIcon icon={['fas', 'home']} />
                                  </span>
                                </Button>
                              </p>
                            </div>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </React.Fragment>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
