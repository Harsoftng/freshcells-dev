import notificationSlice from '../../store/slices/notificationSlice';
import { notifyAction } from '../../store/actions/notify';

export const processResponseError = (error, dispatch) => {
  let title = '',
    msgs = [];

  if (error) {
    if (error?.length > 0) {
      title = '';
      msgs = error?.map(
        (err) =>
          err?.extensions?.exception?.data?.message[0]?.messages[0]?.message ||
          err?.message
      );
    }

    dispatch(notifyAction('', 'error', title, msgs));
  }
};

export const processServerError = (reason, dispatch, error_type = 'error') => {
  let title = '',
    code = 500,
    msgs = ['Invalid response received. Try again!'];

  if (reason?.networkError) {
    console.log('entered');
    if (reason?.networkError?.result?.errors.length > 0) {
      title = reason?.networkError?.name;
      msgs = reason?.networkError?.result?.errors.map(
        (errors) => errors?.message
      );
      code = reason?.networkError?.statusCode;
    }
    dispatch(
      notificationSlice.actions.notify({
        code: code,
        open: true,
        message: reason?.message,
        title: title,
        type: error_type,
        messages: msgs
      })
    );
  }
};
