import React, { useState } from 'react';
import Header from './ui/Header';
import ProfileContent from './ui/ProfileContent';
import FreshCellsTitle from '../components/views/FreshCellsTitle';
import { useDispatch } from 'react-redux';
import { useQuery } from '@apollo/client';
import { USER_ENDPOINT } from '../../api/user/queries';
import { processAPIError } from '../../utils/Utilities';
import { processResponseError } from '../errors/ProcessServerError';
import ProfileSkeleton from '../components/skeletons/ProfileSkeleton';
import FreshCellsNoContentDisplay from '../components/views/FreshCellsNoContentDisplay';
import profileSlice from '../../store/slices/auth/profileSlice';

const ProfilePage = () => {
  const dispatch = useDispatch();
  const [user, setUser] = useState({ id: '' });

  const processAPIData = (data: any) => {
    if (data) {
      if (data.user) {
        setUser(data.user);
        dispatch(
          profileSlice.actions.setProfile({ ...data.user, logged_in: true })
        );
      } else {
        processResponseError(data.user, dispatch);
      }
    }
  };

  const { loading } = useQuery(USER_ENDPOINT, {
    errorPolicy: 'all',
    fetchPolicy: 'no-cache',
    onCompleted: (data) => {
      processAPIData(data);
    },
    onError: (error) => {
      processAPIError(error, dispatch);
    },
    variables: {
      id: 2
    }
  });

  return (
    <>
      <FreshCellsTitle title="Profile" />
      <Header />
      {loading ? (
        <ProfileSkeleton />
      ) : (
        <>
          {user.id ? (
            <ProfileContent user={user} />
          ) : (
            <FreshCellsNoContentDisplay title="Could not retrieve your account details" />
          )}
        </>
      )}
    </>
  );
};

export default ProfilePage;
