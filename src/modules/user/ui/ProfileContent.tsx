import React from 'react';
import {
  Avatar,
  Card,
  CardContent,
  Grid,
  InputAdornment,
  TextField,
  Typography
} from '@material-ui/core';
import { AccountBox } from '@material-ui/icons';
import { IProfileContentProps } from '../types/IProfileContentProps';
import Utilities from '../../../utils/Utilities';

const ProfileContent = ({ user }: IProfileContentProps) => {
  let adornment = {
    startAdornment: (
      <InputAdornment position="start">
        <AccountBox />
      </InputAdornment>
    ),
    readOnly: true
  };

  const avatarNameInitials = `${Utilities.getInitial(
    user.firstName
  )} ${Utilities.getInitial(user.lastName)}`;

  return (
    <Grid
      container
      spacing={2}
      className="d-flex align-items-center justify-content-center v-h-70 mt20 mb20">
      <Grid item lg={4} md={6} sm={6} xs={10}>
        <Card className=" rounded">
          <CardContent className="d-flex align-items-center justify-content-center flex-column p30">
            <Avatar src="" variant="circle" className="h-100 w100">
              {avatarNameInitials}
            </Avatar>

            <Typography variant="h6" className="text-black-50 mt20">
              Welcome {`${user.firstName} ${user.lastName}`}
            </Typography>

            <TextField
              variant="outlined"
              size="medium"
              label="First Name"
              value={user.firstName}
              fullWidth
              placeholder="First Name"
              type="text"
              className="mt20 mb20"
              InputProps={adornment}
            />

            <TextField
              variant="outlined"
              size="medium"
              label="Last Name"
              value={user.lastName}
              fullWidth
              placeholder="Last Name"
              type="text"
              className="mb20"
              InputProps={adornment}
            />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default ProfileContent;
