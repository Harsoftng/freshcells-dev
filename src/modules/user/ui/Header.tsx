import React from 'react';
import { AppBar, Button, Grid, Toolbar } from '@material-ui/core';
import logo from '../../../assets/images/freshcells_logo.svg';
import { PowerSettingsNewOutlined } from '@material-ui/icons';
import { useDispatch } from 'react-redux';
import { logOutAction } from '../../../store/actions/logOut';
import { useFreshCellsConfirmDialog } from '../../../store/context/dialog/FreshCellsConfirmDialogProvider';

const Header = () => {
  const dispatch = useDispatch();

  const { getConfirmation } = useFreshCellsConfirmDialog();

  const logout = async () => {
    const confirmed = await getConfirmation({
      title: 'Attention!',
      type: 'primary',
      okText: 'Proceed',
      cancelText: 'Cancel',
      message: 'Are you sure you would like to logout?'
    });

    if (confirmed) {
      dispatch(logOutAction());
    }
  };

  return (
    <AppBar position="static" color="default">
      <Toolbar className="d-flex align-items-center justify-content-between">
        <Grid>
          <img
            alt="..."
            className="mx-auto d-block img-fluid w100"
            src={logo}
          />
        </Grid>

        <Grid>
          <Button color="inherit" onClick={logout}>
            <PowerSettingsNewOutlined className="mr5" /> Logout
          </Button>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
