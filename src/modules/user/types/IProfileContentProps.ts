import { IProfile } from '../../../store/types/IProfile';

export interface IProfileContentProps {
  user: IProfile;
}
