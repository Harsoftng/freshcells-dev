import React from 'react';

import { Skeleton } from '@material-ui/lab';
import { Card, CardContent, Grid } from '@material-ui/core';

const ProfileSkeleton = () => {
  return (
    <Grid
      container
      spacing={2}
      className="d-flex align-items-center justify-content-center v-h-70">
      <Grid item lg={4} md={6} sm={6} xs={10}>
        <Card className=" rounded">
          <CardContent className="d-flex align-items-center justify-content-center flex-column p30">
            <Skeleton
              variant="circle"
              animation="wave"
              width={100}
              height={100}
              className="mb20"
            />

            <Skeleton
              variant="rect"
              animation="wave"
              height={20}
              width="90%"
              className="mb10"
            />

            <Skeleton
              variant="rect"
              animation="wave"
              height={50}
              width="90%"
              className="mb10 mt40"
            />
            <Skeleton
              variant="rect"
              animation="wave"
              height={50}
              width="90%"
              className="mb30"
            />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default ProfileSkeleton;
