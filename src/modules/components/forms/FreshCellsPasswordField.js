import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { useField } from 'formik';
import { IconButton, InputAdornment, TextField } from '@material-ui/core';
import {
  LockOutlined,
  VisibilityOffRounded,
  VisibilityRounded
} from '@material-ui/icons';
import { useDebouncedCallback } from 'use-debounce';

const FreshCellsPasswordField = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  const [innerValue, setInnerValue] = useState(field.value);

  const [values, setValues] = useState({
    password: '',
    showPassword: false
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const debounced = useDebouncedCallback((event) => {
    if (field.onChange) {
      field.onChange(event);
    }
  }, 200);

  useEffect(() => {
    if (field.value === '') {
      setInnerValue('');
    }
  }, [field.value]);

  const handleOnChange = useCallback(
    (event) => {
      event.persist();
      const newValue = event.currentTarget.value;
      setInnerValue(newValue);
      debounced.callback(event);
    },
    [debounced]
  );

  let adornment = {};
  if (props.icon) {
    adornment = {
      startAdornment: (
        <InputAdornment position="start">
          <LockOutlined />
        </InputAdornment>
      ),
      endAdornment: (
        <InputAdornment position="end">
          <IconButton
            aria-label="Toggle password visibility"
            onClick={handleClickShowPassword}
            onMouseDown={handleMouseDownPassword}>
            {values.showPassword ? (
              <VisibilityRounded />
            ) : (
              <VisibilityOffRounded />
            )}
          </IconButton>
        </InputAdornment>
      )
    };
  } else {
    adornment = {
      endAdornment: (
        <InputAdornment position="end">
          <IconButton
            aria-label="Toggle password visibility"
            onClick={handleClickShowPassword}
            onMouseDown={handleMouseDownPassword}>
            {values.showPassword ? (
              <VisibilityRounded />
            ) : (
              <VisibilityOffRounded />
            )}
          </IconButton>
        </InputAdornment>
      )
    };
  }

  return (
    <React.Fragment>
      <TextField
        fullWidth
        variant="outlined"
        label={label}
        value={innerValue}
        onChange={handleOnChange}
        onBlur={field.onBlur}
        {...props}
        type={values.showPassword ? 'text' : 'password'}
        InputProps={adornment}
      />
      {meta.touched && meta.error ? (
        <div className="oak-input-error">{meta.error}</div>
      ) : null}
    </React.Fragment>
  );
};

FreshCellsPasswordField.propTypes = {
  label: PropTypes.string.isRequired
};

export default FreshCellsPasswordField;
