import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { useField } from 'formik';
import { InputAdornment, TextField } from '@material-ui/core';
import { AccountBox } from '@material-ui/icons';
import { useDebouncedCallback } from 'use-debounce';

const FreshCellsTextField = ({ label, readOnly, ...props }) => {
  const [field, meta] = useField(props);
  const [innerValue, setInnerValue] = useState(field.value);

  const debounced = useDebouncedCallback((event) => {
    if (field.onChange) {
      field.onChange(event);
    }
  }, 200);

  useEffect(() => {
    if (field.value === '') {
      setInnerValue('');
    } else {
      setInnerValue(field.value);
    }
  }, [field.value]);

  const handleOnChange = useCallback(
    (event) => {
      event.persist();
      const newValue = event.currentTarget.value;
      setInnerValue(newValue);
      debounced.callback(event);
    },
    [debounced]
  );

  let adornment = {};
  if (props.icon) {
    adornment = {
      startAdornment: (
        <InputAdornment position="start">
          {props.icon ? props.icon : <AccountBox />}
        </InputAdornment>
      ),
      readOnly: readOnly
    };
  }

  return (
    <React.Fragment>
      <TextField
        fullWidth
        variant="outlined"
        label={label}
        value={innerValue}
        onChange={handleOnChange}
        onBlur={field.onBlur}
        {...props}
        InputProps={adornment}
      />
      {meta.touched && meta.error ? (
        <div className="oak-input-error">{meta.error}</div>
      ) : null}
    </React.Fragment>
  );
};

FreshCellsTextField.propTypes = {
  label: PropTypes.string.isRequired,
  readOnly: PropTypes.bool
};

FreshCellsTextField.defaultProps = {
  readOnly: false
};

export default FreshCellsTextField;
