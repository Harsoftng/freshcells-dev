import React from 'react';
import { Snackbar } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { useDispatch, useSelector } from 'react-redux';
import Slide from '@material-ui/core/Slide';
import notificationSlice from '../../../store/slices/notificationSlice';
import { IStoreStateProps } from '../../../store/types/IStoreStateProps';

function SlideTransition(props: any) {
  return <Slide {...props} direction="up" />;
}

export default function FreshCellsSnackbar() {
  const dispatch = useDispatch();
  const notifications = useSelector(
    (state: IStoreStateProps) => state.notifications
  );

  const isSingleError = notifications.messages.length < 1;

  const handleClose = () => {
    dispatch(notificationSlice.actions.clearNotification({}));
  };

  return (
    <React.Fragment>
      <Snackbar
        autoHideDuration={30000}
        open={notifications.open}
        className="text-white"
        TransitionComponent={SlideTransition}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        onClose={handleClose}>
        <Alert
          onClose={handleClose}
          variant="filled"
          className="text-white"
          severity={notifications.type || 'error'}>
          {notifications.title !== '' && (
            <AlertTitle className="fs18">
              {notifications.title || ''}
            </AlertTitle>
          )}

          {!isSingleError ? (
            <React.Fragment>
              <ul className="mr20 pl10">
                {notifications.messages.map((item, id) => (
                  <li key={id} className="s16 ml10 ">
                    {item}
                  </li>
                ))}
              </ul>
            </React.Fragment>
          ) : (
            <div className="fs16 mr20">{notifications.message}</div>
          )}
        </Alert>
      </Snackbar>
    </React.Fragment>
  );
}
