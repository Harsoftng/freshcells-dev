import React from 'react';
import { Helmet } from 'react-helmet';
import { IFreshCellsTitleProps } from '../types/IFreshCellsTitleProps';

const FreshCellsTitle = ({
  title,
  separator,
  children
}: IFreshCellsTitleProps) => {
  let pageTitle: string =
    title + (separator || ' - ') + process.env.REACT_APP_COMPANY_NAME;

  return (
    <React.Fragment>
      <Helmet>
        <title>{pageTitle}</title>
        {children}
      </Helmet>
    </React.Fragment>
  );
};

export default FreshCellsTitle;
