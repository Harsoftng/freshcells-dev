import React from 'react';

const DeviceNotSupported = () => {
  return (
    <div>
      <div
        style={{
          position: 'relative',
          minHeight: '-webkit-calc(100% - 44px)',
          background: '#f3f4f5',
          height: '100vh'
        }}>
        <div style={{ padding: '30px 30px' }}>
          <div
            style={{
              marginBottom: '22px',
              backgroundColor: '#fff',
              border: '1px solid transparent',
              borderRadius: '4px',
              WebkitBoxShadow: '0 1px 1px rgba(0, 0, 0, .05)',
              boxShadow: '0 1px 1px rgba(0, 0, 0, .05)',
              maxWidth: '660px',
              margin: '0 auto'
            }}>
            <div
              style={{
                padding: '30px 30px',
                marginRight: 'auto',
                marginLeft: 'auto'
              }}>
              <div style={{ padding: '15px 0 5px', textAlign: 'center' }}>
                <img
                  src="/freshcells_logo.svg"
                  width="200"
                  height="60"
                  alt="logo"
                />
                <h4 style={{ color: '#546e7a !important', fontWeight: 'bold' }}>
                  Fresh Cells App
                </h4>
              </div>
              <div
                style={{
                  padding: '20px 0',
                  margin: 0,
                  position: 'relative',
                  marginBottom: '30px',
                  backgroundColor: '#fff',
                  borderRadius: '3px'
                }}>
                <div
                  style={{
                    borderRadius: '0 0 3px 3px',
                    position: 'relative',
                    padding: '0px 25px'
                  }}>
                  <h1
                    style={{
                      fontFamily: 'Avenir, Helvetica, sans-serif',
                      boxSizing: 'border-box',
                      color: '#2F3133',
                      fontSize: '19px',
                      fontWeight: 'bold',
                      marginTop: 0,
                      textAlign: 'left'
                    }}>
                    Your Browser is not supported
                  </h1>
                  <p>
                    We are unable to support your browser at the moment, please
                    upgrade or download other browsers!
                  </p>
                  <div>
                    <strong>
                      <big>
                        Please find below the list of supported browsers:
                      </big>
                    </strong>
                    <ol>
                      <li>Google Chrome Browser (Version 61 and above)</li>
                      <li>Mozilla Firefox Browser (Version 51 and above)</li>
                      <li>Opera Browser (Version 50 and above)</li>
                      <li>Safari Browser (Version 12 and above)</li>
                      <li>
                        Microsoft Internet Explorer Browser (Version 11 and
                        above)
                      </li>
                      <li>Microsoft Edge Browser (Version 15 and above)</li>
                      <li>
                        And other new versions of recent modern browsers
                        including all mobile device browsers
                      </li>
                    </ol>
                  </div>

                  <div style={{ margin: '0 0 11px' }}>
                    To view and enjoy the full features of this site, it is
                    highly recommended that you upgrade your browser to the
                    latest version or download the latest version of the
                    supported browsers listed above. FInd below links to upgrade
                    or download the latest versions of chrome, firefox and
                    internet explorer
                    <ul className="">
                      <li className="chrome">
                        <a
                          style={{ color: '#00F' }}
                          href="https://support.google.com/chrome/answer/95414?co=GENIE.Platform%3DDesktop&hl=en"
                          target="_blank"
                          rel="noopener">
                          Upgrade Chrome
                        </a>
                      </li>
                      <li className="firefox">
                        <a
                          style={{ color: '#00F' }}
                          href="https://support.mozilla.org/en-US/kb/update-firefox-latest-version"
                          target="_blank"
                          rel="noopener">
                          Upgrade Firefox
                        </a>
                      </li>
                      <li className="safari">
                        <a
                          style={{ color: '#00F' }}
                          href="https://support.apple.com/en-us/HT204416"
                          target="_blank"
                          rel="noopener">
                          Upgrade Safari
                        </a>
                      </li>
                      <li className="ie">
                        <a
                          style={{ color: '#00F' }}
                          href="https://support.microsoft.com/en-us/help/17621/internet-explorer-downloads"
                          target="_blank"
                          rel="noopener">
                          Upgrade IE
                        </a>
                      </li>
                      <li className="firefox safari ie edge">
                        <a
                          style={{ color: '#00F' }}
                          href="https://www.google.com/intl/en/chrome/"
                          target="_blank"
                          rel="noopener">
                          Download Chrome
                        </a>
                      </li>
                      <li className="chrome safari ie edge">
                        <a
                          style={{ color: '#00F' }}
                          href="https://www.mozilla.org/en-US/firefox/new/"
                          target="_blank"
                          rel="noopener">
                          Download Firefox
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeviceNotSupported;
