import React from 'react';
import defaultIcon from '../../../assets/images/illustrations/pack5/no_data_illustration.jpg';
import { Grid } from '@material-ui/core';
import { IFreshCellsNoContentDisplayProps } from '../types/IFreshCellsNoContentDisplayProps';

const FreshCellsNoContentDisplay = ({
  title,
  subTitle,
  illustrationIcon,
  viewHeight
}: IFreshCellsNoContentDisplayProps) => {
  return (
    <Grid
      className="d-flex justify-content-center align-items-center bg-white"
      style={{ height: viewHeight || '90vh' }}>
      <Grid item md={12} xl={12} className="mx-auto text-center">
        <Grid className="">
          <img
            src={illustrationIcon || defaultIcon}
            className="mx-auto d-block mb-1 img-responsive"
            alt={title || 'No Data Found'}
            width={250}
          />

          <h2 className="mb-2 px-4 font-weight-bold">
            {title || 'No Data Found'}
          </h2>
          <p className="font-size-lg line-height-sm font-weight-light d-block px-3 mb-3 text-black-50 mb50">
            {subTitle || 'Please perform an action to display data here'}
          </p>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default FreshCellsNoContentDisplay;
