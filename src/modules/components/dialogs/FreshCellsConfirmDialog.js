import React from 'react';
import PropTypes from 'prop-types';
import { Button, Dialog } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import clsx from 'clsx';

export default function FreshCellsConfirmDialog({
  type,
  title,
  message,
  open = false,
  okText = 'Ok',
  cancelText = 'Cancel',
  onOkClick = (f) => f,
  onCancelClick = (f) => f
}) {
  let iconWrapperClass = '',
    iconClass = '',
    cancelButtonClass = '',
    okButtonClass = '';

  switch (type) {
    case 'warning':
      iconWrapperClass = 'bg-warning text-white';
      iconClass = 'dot-circle';
      cancelButtonClass = 'btn-neutral-secondary';
      okButtonClass = 'btn-outline-warning';
      break;
    case 'info':
      iconWrapperClass = 'bg-neutral-first text-first';
      iconClass = 'keyboard';
      cancelButtonClass = 'bg-neutral-secondary text-danger';
      okButtonClass = 'btn-first';
      break;
    case 'error':
      iconWrapperClass = 'bg-neutral-danger text-danger';
      iconClass = 'times';
      cancelButtonClass = 'btn-neutral-secondary';
      okButtonClass = 'btn-danger';
      break;
    case 'success':
      iconWrapperClass = 'bg-neutral-success text-success';
      iconClass = 'lightbulb';
      cancelButtonClass = 'btn-neutral-dark ';
      okButtonClass = 'btn-success';
      break;
    default:
      iconWrapperClass = 'bg-neutral-first text-first';
      iconClass = 'keyboard';
      cancelButtonClass = 'bg-neutral-secondary text-danger';
      okButtonClass = 'btn-first';
      break;
  }

  return (
    <React.Fragment>
      <Dialog
        open={open}
        onClose={onCancelClick}
        classes={{ paper: 'shadow-lg rounded' }}>
        <div className="text-center p-5">
          <div className="avatar-icon-wrapper rounded-circle m-0">
            <div
              className={clsx(
                'd-inline-flex justify-content-center',
                'p-0 rounded-circle btn-icon ',
                'avatar-icon-wrapper m-0 d-130',
                iconWrapperClass
              )}>
              <FontAwesomeIcon
                icon={['fas', iconClass]}
                className="d-flex align-self-center display-3"
              />
            </div>
          </div>
          <h4 className="font-weight-bold mt-4">{title}</h4>
          <p className="mb-0 font-size-lg text-black-50">{message}</p>
          <div className="pt-4">
            <Button
              onClick={onCancelClick}
              color="default"
              variant="text"
              className={clsx('btn-pill mx-1', cancelButtonClass)}>
              <span className="btn-wrapper--label">{cancelText}</span>
            </Button>
            <Button
              onClick={onOkClick}
              color="primary"
              variant="contained"
              className={clsx('btn-pill mx-1', okButtonClass)}>
              <span className="btn-wrapper--label">{okText}</span>
            </Button>
          </div>
        </div>
      </Dialog>
    </React.Fragment>
  );
}
FreshCellsConfirmDialog.propTypes = {
  type: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  open: PropTypes.bool,
  onOkClick: PropTypes.func,
  onCancelClick: PropTypes.func
};
