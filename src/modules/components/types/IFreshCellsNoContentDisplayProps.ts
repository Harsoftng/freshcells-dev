export interface IFreshCellsNoContentDisplayProps {
  title: string;
  subTitle?: string;
  viewHeight?: string;
  illustrationIcon?: string;
}
