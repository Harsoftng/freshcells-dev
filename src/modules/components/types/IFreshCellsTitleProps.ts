import { ReactNode } from 'react';

export interface IFreshCellsTitleProps {
  title: string;
  separator?: string;
  children?: ReactNode;
}
