// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';
import Utilities from './utils/Utilities';

test('test the isDeviceSupported function', () => {
  expect(Utilities.isDeviceSupported()).toBeFalsy();
});

test('test the getInitial function', () => {
  expect(Utilities.getInitial('Ibrahim')).toBe('I');
});

test('test Base64 Encoding and Decoding', () => {
  expect(Utilities.base64Encode('FreshCells')).toBe('RnJlc2hDZWxscw==');
  expect(Utilities.base64Decode('RnJlc2hDZWxscw==')).toBe('FreshCells');
});

test('', () => {});
