import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import storeFactory from './store';
import { Provider } from 'react-redux';
import Routes from './routes/Routes';

import '../src/assets/css/helpers.css';
import {
  ApolloProvider,
  ApolloClient,
  InMemoryCache,
  concat,
  ApolloLink,
  NormalizedCacheObject
} from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import { createUploadLink } from 'apollo-upload-client';
import { notifyAction } from './store/actions/notify';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  far,
  faTimesCircle,
  faDotCircle,
  faKeyboard,
  faLightbulb
} from '@fortawesome/free-regular-svg-icons';
import { fas, faInfoCircle, faTimes } from '@fortawesome/free-solid-svg-icons';

library.add(
  far,
  faTimesCircle,
  faDotCircle,
  faKeyboard,
  faLightbulb,
  fas,
  faInfoCircle,
  faTimes
);

const initialState = localStorage['app-store']
  ? JSON.parse(localStorage['app-store'])
  : {};

const store = storeFactory(initialState);
store.subscribe(() => {
  localStorage['app-store'] = JSON.stringify(store.getState());
});

let apiUrl: string = 'https://cms.trial-task.k8s.ext.fcse.io/graphql';

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  (window as any).store = store;
}

const logOutLink: ApolloLink = onError(({ graphQLErrors, networkError }) => {
  if (networkError) {
    store.dispatch(
      notifyAction(
        'Failed to connect to the server, please check your internet connection',
        'warning'
      )
    );
  }
});

const httpLink: ApolloLink = createUploadLink({
  uri: apiUrl
});

const authMiddleware: ApolloLink = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  const headers = !store.getState().token
    ? {}
    : {
        authorization: `Bearer ${store.getState().token}`
      };

  operation.setContext({
    headers: headers
  });

  return forward(operation);
});

const apolloClient: ApolloClient<NormalizedCacheObject> = new ApolloClient({
  link: concat(logOutLink.concat(authMiddleware), httpLink),
  cache: new InMemoryCache()
});

function App() {
  return (
    <Provider store={store}>
      <ApolloProvider client={apolloClient}>
        <BrowserRouter basename="/">
          <Routes />
        </BrowserRouter>
      </ApolloProvider>
    </Provider>
  );
}

export default App;
