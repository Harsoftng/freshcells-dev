import ThemeOptions from './reducers';
import {
  combineReducers,
  configureStore,
  getDefaultMiddleware
} from '@reduxjs/toolkit';
import tokenSlice from './slices/auth/tokenSlice';
import profileSlice from './slices/auth/profileSlice';
import notificationSlice from './slices/notificationSlice';

const appMiddleware = (store, dispatch) => (next) => (action) => {
  return next(action);
};

const appReducers = combineReducers({
  ThemeOptions,
  token: tokenSlice.reducer,
  profile: profileSlice.reducer,
  notifications: notificationSlice.reducer
});

export default function storeFactory(initialState = {}) {
  return configureStore({
    reducer: appReducers,
    middleware: [appMiddleware, ...getDefaultMiddleware()],
    preloadedState: initialState
  });
}
