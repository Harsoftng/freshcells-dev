import { DefaultRootState } from 'react-redux';
import { IProfile } from './IProfile';

export interface IStoreStateProps extends DefaultRootState {
  token: string;
  notifications: INotificationState;
  profile: IProfile;
}

export interface INotificationState {
  code: number;
  open: boolean;
  message: string;
  title: string;
  type: any;
  messages: string[];
}
