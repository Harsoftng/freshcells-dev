export interface IProfile {
  id?: string;
  email?: string;
  lastName?: string;
  firstName?: string;
  logged_in?: boolean;
}
