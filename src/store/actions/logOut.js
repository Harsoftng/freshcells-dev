import React from 'react';
import profileSlice from '../slices/auth/profileSlice';
import notificationSlice from '../slices/notificationSlice';
import tokenSlice from '../slices/auth/tokenSlice';
import { Redirect } from 'react-router-dom';

export const logOutAction = () => (dispatch) => {
  dispatch(profileSlice.actions.clearProfile());
  dispatch(notificationSlice.actions.clearNotification());
  dispatch(tokenSlice.actions.clearToken());
  localStorage.clear();
  return <Redirect to="/account" />;
};
