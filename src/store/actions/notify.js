import notificationSlice from '../slices/notificationSlice';

export const notifyAction = (
  message,
  type = 'info',
  title = '',
  messages = []
) => (dispatch) => {
  dispatch(
    notificationSlice.actions.notify({
      code: 0,
      open: true,
      message: message,
      title: title,
      type: type,
      messages: messages
    })
  );
};
