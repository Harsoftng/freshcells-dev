//Set Entire Theme

export const SET_THEME = 'THEME_OPTIONS/SET_THEME';
export const setTheme = (theme) => ({
  type: SET_THEME,
  payload: theme
});

export default function reducer(
  state = {
    contentBackground: ''
  },
  action
) {
  switch (action.type) {
    // Sidebar

    case SET_THEME:
      return action.payload;

    default:
      break;
  }
  return state;
}
