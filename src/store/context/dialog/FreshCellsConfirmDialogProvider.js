import React, { useState } from 'react';
import FreshCellsConfirmDialog from '../../../modules/components/dialogs/FreshCellsConfirmDialog';

export const FreshCellsConfirmDialogContext = React.createContext({});

const defaultParams = {
  title: '',
  type: '',
  okText: '',
  cancelText: '',
  message: '',
  actionCallback: () => {}
};

export const FreshCellsConfirmDialogProvider = ({ children }) => {
  const [dialogOpen, setDialogOpen] = useState(false);
  const [dialogConfig, setDialogConfig] = useState(defaultParams);

  const openDialog = ({
    title,
    type,
    okText,
    cancelText,
    message,
    actionCallback
  }) => {
    setDialogOpen(true);
    setDialogConfig({
      title,
      type,
      okText,
      cancelText,
      message,
      actionCallback
    });
  };

  const closeDialog = () => {
    setDialogOpen(false);
    setDialogConfig(defaultParams);
  };

  const onConfirm = () => {
    closeDialog();
    dialogConfig.actionCallback(true);
  };

  const onDismiss = () => {
    closeDialog();
    dialogConfig.actionCallback(false);
  };

  return (
    <FreshCellsConfirmDialogContext.Provider
      value={{ openDialog, dialogConfig, onDismiss }}>
      <FreshCellsConfirmDialog
        open={dialogOpen}
        title={dialogConfig.title}
        type={dialogConfig.type}
        okText={dialogConfig.okText}
        cancelText={dialogConfig.cancelText}
        message={dialogConfig.message}
        onOkClick={onConfirm}
        onCancelClick={onDismiss}
      />
      {children}
    </FreshCellsConfirmDialogContext.Provider>
  );
};

export const useFreshCellsConfirmDialog = () => {
  const { openDialog, onDismiss } = React.useContext(
    FreshCellsConfirmDialogContext
  );

  const getConfirmation = ({ ...options }) =>
    new Promise((res) => {
      openDialog({ actionCallback: res, ...options });
    });

  const closeDialog = () =>
    new Promise(() => {
      onDismiss();
    });

  return { getConfirmation, closeDialog };
};
