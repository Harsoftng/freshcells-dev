import { createSlice } from '@reduxjs/toolkit';

const tokenSlice = createSlice({
  name: 'token',
  initialState: '',
  reducers: {
    setToken(state = '', action) {
      return action.payload;
    },
    clearToken(state = '', action) {
      return '';
    }
  }
});

export const { actions: tokenActions, reducer: tokenReducers } = tokenSlice;

export default tokenSlice;
