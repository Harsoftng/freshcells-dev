import { createSlice } from '@reduxjs/toolkit';

const initState = {
  id: 0,
  email: '',
  lastName: '',
  firstName: '',
  logged_in: false
};

const profileSlice = createSlice({
  name: 'profile',
  initialState: initState,
  reducers: {
    setProfile(state = {}, action) {
      return action.payload;
    },
    setLoggedIn(state = false, action) {
      state.logged_in = true;
    },
    clearProfile(state = {}, action) {
      return initState;
    }
  }
});

export const {
  actions: profileActions,
  reducer: profileReducers
} = profileSlice;

export default profileSlice;
