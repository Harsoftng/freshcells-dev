import { createSlice } from '@reduxjs/toolkit';

const initState = {
  code: 0,
  open: false,
  message: '',
  title: '',
  type: '',
  messages: []
};

const notificationSlice = createSlice({
  name: 'notifications',
  initialState: initState,
  reducers: {
    notify(state = initState, action) {
      return action.payload;
    },
    clearNotification(state, action) {
      state.open = false;
    }
  }
});

export const {
  actions: errorActions,
  reducer: errorReducers
} = notificationSlice;

export default notificationSlice;
