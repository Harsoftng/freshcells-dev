import { gql } from '@apollo/client';

export const USER_ENDPOINT = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      email
      firstName
      lastName
    }
  }
`;
