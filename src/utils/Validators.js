import * as Yup from 'yup';

export const validateEmail = (required = true, min = 10, max = 255) => {
  let schema = Yup.string()
    .email('Invalid email address!')
    .min(min, 'Email addresses must be 10 characters or more!')
    .max(max, 'Email addresses must be 255 characters or less!');
  if (required) {
    schema = schema.required('A valid email is required!');
  }
  return schema;
};

export const validateString = (
  required = true,
  min = 10,
  max = 255,
  message = 'A value is required!'
) => {
  let schema = Yup.string()
    .min(min, 'Value must be ' + min + ' characters or more!')
    .max(max, 'Value must be ' + max + ' characters or less!');
  if (required) {
    schema = schema.required(message);
  }
  return schema;
};

export const validateUpload = (
  msg = 'A value is required!',
  required = true
) => {
  if (required) {
    return Yup.object().shape({
      id: Yup.string().required(msg),
      url: Yup.string().required(msg)
    });
  } else {
    return Yup.object().shape({
      id: Yup.string(),
      url: Yup.string()
    });
  }
};

export const validateAutoComplete = (
  msg = 'Please select a value!',
  required = true
) => {
  if (required) {
    return Yup.object().shape({
      value: Yup.string().required(msg),
      text: Yup.string().required(msg)
    });
  } else {
    return Yup.object().shape({
      value: Yup.string(),
      text: Yup.string()
    });
  }
};

export const validateSelect = (required = true, min = 10, max = 255) => {
  let schema = Yup.string();
  if (required) {
    schema = schema.required('Please select a value!');
  }
  return schema;
};

export const validateNumber = (required = true, min = 10, max = 255) => {
  let schema = Yup.number()
    .typeError('A valid number is required!')
    .min(min, 'Value must be greater than ' + min)
    .max(max, 'Value must be lesser than ' + max);
  if (required) {
    schema = schema.required('A valid number is required!');
  }
  return schema;
};

export const validateLongNumber = (required = true) => {
  let schema = Yup.number().typeError('A valid number is required!');
  if (required) {
    schema = schema.required('A valid number is required!');
  }
  return schema;
};

export const validateMoney = (required = true) => {
  const moneyRegExp = /^[0-9]\d*(((,\d{3}){1})*(\.\d{0,2})?)$/;
  let schema = Yup.string().matches(moneyRegExp, 'A valid amount is required!');
  if (required) {
    schema = schema.required('A valid amount is required!');
  }
  return schema;
};

export const validatePhone = (required = true) => {
  const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
  let schema = Yup.string().matches(phoneRegExp, 'Phone number is not valid');
  if (required) {
    schema = schema.required('A valid phone number is required!');
  }
  return schema;
};

export const validatePassword = (
  useStrict = true,
  required = true,
  min = 6,
  max = 50
) => {
  let schema = Yup.string()
    .min(min, 'Passwords must be 6 characters or more!')
    .max(max, 'Passwords must be 50 characters or less!');
  if (useStrict) {
    schema = schema.matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$/,
      'Passwords must contain at least 6 characters consisting of at ' +
        'least one uppercase, one lowercase, one number and one special' +
        ' case character'
    );
  }
  if (required) {
    schema = schema.required('A valid password is required');
  }

  return schema;
};

export const validatePasswordConfirm = (
  fieldName = 'password',
  required = true
) => {
  let schema = Yup.string().oneOf(
    [Yup.ref(fieldName), null],
    'Passwords must match'
  );

  if (required) {
    schema = schema.required('Please confirm your password!');
  }

  return schema;
};
