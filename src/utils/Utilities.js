import {
  processResponseError,
  processServerError
} from '../modules/errors/ProcessServerError';
import { notifyAction } from '../store/actions/notify';
import * as detectDevice from 'react-device-detect';
import { encode as base64_encode, decode as base64_decode } from 'base-64';

const base64Encode = (text) => {
  let words = '';
  if (text) {
    words = base64_encode(text);
  }
  return words;
};

const base64Decode = (text) => {
  let words = '';
  if (text) {
    words = base64_decode(text);
  }
  return words;
};

export const processAPIError = (error, dispatch) => {
  processServerError(error, dispatch);
};

export const processAPIResponse = (data, dispatch) => {
  if (data) {
    if (data.response.status === 'success') {
      dispatch(notifyAction(data.response.message, 'success'));
    } else {
      processResponseError(data.response, dispatch);
    }
  }
};

const isDeviceSupported = () => {
  let supported = false;

  if (detectDevice.isBrowser) {
    if (detectDevice.isChrome && detectDevice.browserVersion >= 61) {
      supported = true;
    } else if (detectDevice.isEdge && detectDevice.browserVersion >= 15) {
      supported = true;
    } else if (detectDevice.isIE && detectDevice.browserVersion >= 11) {
      supported = true;
    } else if (detectDevice.isFirefox && detectDevice.browserVersion >= 51) {
      supported = true;
    } else if (detectDevice.isOpera && detectDevice.browserVersion >= 50) {
      supported = true;
    } else if (detectDevice.isSafari && detectDevice.browserVersion >= 12) {
      supported = true;
    }
  } else {
    supported = true;
  }
  return supported;
};

export const getInitial = (text) => {
  let initial = '';
  if (text) {
    initial = text.substr(0, 1).toUpperCase();
  }
  return initial;
};

const Utilities = {
  processAPIError,
  processAPIResponse,
  isDeviceSupported,
  base64Decode,
  base64Encode,
  getInitial
};
export default Utilities;
