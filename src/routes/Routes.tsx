import React, { Suspense } from 'react';
import {
  Switch,
  Route,
  useHistory,
  useLocation,
  Redirect
} from 'react-router-dom';
import { AnimatePresence, motion, Variants } from 'framer-motion';
import { ThemeProvider } from '@material-ui/styles';
import MuiTheme from '../theme';
import SuspenseLoading from '../modules/components/views/SuspenseLoading';
import AuthRoutes from './AuthRoutes';
import ErrorPage404 from '../modules/errors/ErrorPage404';
import ErrorBoundary from '../modules/errors/ErrorBoundary';
import { FreshCellsConfirmDialogProvider } from '../store/context/dialog/FreshCellsConfirmDialogProvider';
import UserRoutes from './UserRoutes';
import FreshCellsSnackbar from '../modules/components/notifications/FreshCellsSnackbar';

const Routes = () => {
  const history = useHistory();
  const location = useLocation();

  const pageVariants: Variants = {
    initial: {
      opacity: 0
    },
    in: {
      opacity: 1
    },
    out: {
      opacity: 0
    }
  };

  const pageTransition: object = {
    type: 'tween',
    ease: 'linear',
    duration: 0.3
  };

  return (
    <ThemeProvider theme={MuiTheme}>
      <FreshCellsSnackbar />
      <AnimatePresence>
        <Suspense fallback={<SuspenseLoading />}>
          <FreshCellsConfirmDialogProvider>
            <ErrorBoundary history={history}>
              <Switch location={location} key={location.pathname}>
                <Redirect exact from="/" to="/login" />
                <Route path={['/login']}>
                  <motion.div
                    initial="initial"
                    animate="in"
                    exit="out"
                    variants={pageVariants}
                    transition={pageTransition}>
                    <AuthRoutes
                      pageVariants={pageVariants}
                      pageTransition={pageTransition}
                    />
                  </motion.div>
                </Route>

                <Route path={['/account']}>
                  <motion.div
                    initial="initial"
                    animate="in"
                    exit="out"
                    variants={pageVariants}
                    transition={pageTransition}>
                    <UserRoutes />
                  </motion.div>
                </Route>

                <Route component={ErrorPage404} />
              </Switch>
            </ErrorBoundary>
          </FreshCellsConfirmDialogProvider>
        </Suspense>
      </AnimatePresence>
    </ThemeProvider>
  );
};

export default Routes;
