import React, { lazy } from 'react';
import { Switch, useLocation, withRouter } from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute';

const UserProfile = lazy(() => import('../modules/user/ProfilePage'));

const UserRoutes = () => {
  const location = useLocation();
  return (
    <Switch location={location} key={location.pathname}>
      <ProtectedRoute exact path="/account" Component={UserProfile} />
    </Switch>
  );
};

export default withRouter(UserRoutes);
