import { Variants } from 'framer-motion';
import { RouteComponentProps } from 'react-router-dom';

export interface IAuthRoutesProps extends RouteComponentProps {
  location: any;
  pageVariants: Variants;
  pageTransition: object;
}
