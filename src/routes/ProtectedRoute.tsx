import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { differenceInHours } from 'date-fns';
import { Route, Redirect } from 'react-router-dom';
import { logOutAction } from '../store/actions/logOut';
import { IStoreStateProps } from '../store/types/IStoreStateProps';

// @ts-ignore
const ProtectedRoute = ({ Component, ...rest }) => {
  //check for authentication, after each route render

  const dispatch = useDispatch();
  const token = useSelector((state: IStoreStateProps) => state.token);
  const profile = useSelector((state: IStoreStateProps) => state.profile);

  let isAuthenticated: boolean | undefined =
    token !== '' && token.length > 50 && profile.logged_in;

  useEffect(() => {
    if (!localStorage['user-last-seen']) {
      localStorage['user-last-seen'] = Date.now();
    } else {
      const last_seen = new Date(parseInt(localStorage['user-last-seen']));
      const hours: number = differenceInHours(last_seen, Date.now());
      if (Math.abs(hours) >= 2) {
        dispatch(logOutAction());
      }
    }

    // eslint-disable-next-line
  }, []);

  if (!isAuthenticated) {
    console.log('clearing local storage');
    dispatch(logOutAction());
  }

  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
};

export default ProtectedRoute;
