import React, { lazy } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { IAuthRoutesProps } from './types/IAuthRoutesProps';
const LoginPage = lazy(() => import('../modules/auth/LoginPage'));

const AuthRoutes = ({ location }: IAuthRoutesProps) => {
  return (
    <Switch location={location} key={location.pathname}>
      <Route exact path="/login" component={LoginPage} />
    </Switch>
  );
};

export default withRouter(AuthRoutes);
